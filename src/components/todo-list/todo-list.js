import React from 'react';
import TodoListItem from '../todo-list-item'
import './todo-list.css'

const TodoList = ({todos,toggleImportant}) => {
    const elements = todos.map((item) => {
  
      return (
        <li key={item.id} className = "list-group-item">
          <TodoListItem toggleImportant = {toggleImportant} {...item}/> 
          </li>
      );
    });
  
    return (
      <div>
        <ul className='list-group todo-list'>
          {elements}
        </ul>
      </div>
    );
  };

  export default TodoList;